// Based on Straight curve by Bridget Riley (1963)
// https://diaart.org/media/w1050h700/object/straight-curve-1963-2.jpg
// MIT License - Nicolas Lebrun - @nclslbrn

let straightCurve = function (p) {
  const sketchWidth = 600;
  const sketchHeight = 720;
  const cells = 25;

  const sketchSize = (sketchWidth, sketchHeight) => {
    const ratio = sketchWidth / sketchHeight;
    const side = Math.min(window.innerWidth, window.innerHeight);
    return {
      w: side > 800 ? sketchWidth : side * ratio,
      h: side > 800 ? sketchHeight : side,
    };
  };

  const canvasSize = sketchSize(sketchWidth, sketchHeight);
  const regularSize = canvasSize.w / cells;
  const cellSizeVariation = (regularSize / cells) * 4;

  let xIncrement,
    yIncrement,
    xIncrementStep,
    yIncrementStep,
    initStep,
    xIncrementFactor,
    yIncrementFactor;
  let isFading = false,
    fadeCount = 0,
    cellWidth,
    cellHeight,
    xPos,
    yPos;

  const reInitialize = function () {
    xIncrement = yIncrement = -cellSizeVariation;
    cellWidth = regularSize + xIncrement;
    cellHeight = regularSize / 5;
    initStep = (cellSizeVariation / cells) * regularSize - 1;
    xIncrementFactor = 1.497;
    yIncrementFactor = 0.85;
    xIncrementStep = initStep * xIncrementFactor;
    yIncrementStep = initStep * yIncrementFactor;
    xPos = yPos = 0;
  };

  p.setup = function () {
    p.createCanvas(canvasSize.w, canvasSize.h);
    p.background(250);
    p.fill(25);
    reInitialize();
  };

  p.draw = function () {
    if (isFading) {
      if (fadeCount > 160) {
        isFading = false;
        fadeCount = 0;
        p.background(250);
        reInitialize();
      } else {
        // Opacity should be one but the painting don't vanish at one
        p.background(250, fadeCount / 10);
        fadeCount++;
      }
    } else {
      if (xPos < sketchWidth) {
        if (yPos < sketchHeight) {
          if (yIncrement > cellSizeVariation) yIncrementStep--;

          if (yIncrement < -cellSizeVariation) yIncrementStep++;

          yIncrement += yIncrementStep;

          cellHeight = regularSize + yIncrement;

          p.triangle(
            xPos,
            yPos,
            xPos + cellWidth,
            yPos + cellHeight,
            xPos,
            yPos + cellHeight
          );

          yPos = yPos + cellHeight;
        } else {
          // end of column

          yIncrementStep = initStep * yIncrementFactor;
          yIncrement = -cellSizeVariation;
          yPos = 0;

          if (xIncrement > cellSizeVariation) xIncrementStep--;

          if (xIncrement < -cellSizeVariation) xIncrementStep++;

          xIncrement += xIncrementStep;
          cellWidth = regularSize + xIncrement;
          xPos = xPos + cellWidth;
        }
      } else {
        isFading = true;
        fadeCount = 0;
        p.noLoop();
        setTimeout(function () {
          p.loop();
          p.redraw();
        }, 6900);
      }
    }
  };
};

new p5(straightCurve, window.document.getElementById("straight-curve"));
