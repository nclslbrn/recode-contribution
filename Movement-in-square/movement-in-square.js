// This is a ugly attempt to recreate Movement in square by Bridget Riley (1961)
// https://www.moma.org/collection/works/75869
// MIT License - Nicolas Lebrun - @nclslbrn

let movementInSquare = function (p) {

  const screenSize = Math.min(window.innerWidth, window.innerHeight),
    sketchSize = screenSize > 720 ? 720 : screenSize * 0.9,
    cols = 31,
    rows = 12,
    colors = [25, 250]

  let isFading = false, fadeCount = 0, x, y, dx, cellW, cellH

  const reInitialize = function () {
    x = 0
    y = 0
    dx = 0
    cellH = sketchSize / (rows + 1)
    cellW = cellH
  }
  p.setup = function () {
    p.createCanvas(sketchSize, sketchSize)
    p.fill(colors[0])
    p.noStroke()
    p.background(colors[1])
    reInitialize()
  }


  p.draw = function () {
    if (isFading) {

      if (fadeCount > 160) {
        isFading = false
        fadeCount = 0
        p.background(colors[1])
        reInitialize();
      } else {
        // Opacity should be one but the painting don't vanish at one
        p.background(colors[1], fadeCount / 10)
        fadeCount++
      }

    } else {

      if (x <= cols) {
        if (y <= rows) {

          if ((x + y) % 2 === 0) {
            p.rect(dx, y * cellH, cellW, cellH)
          }
          y++
        } else {
          dx += cellW
          y = 0
          if (x <= cols / 2) {
            cellW *= 0.85;
          } else {
            cellW *= 1.2055;
          }
          x++
        }

      } else {
        isFading = true
        fadeCount = 0

        // Pause the sketch
        p.noLoop();
        setTimeout(function () {
          p.loop();
          p.redraw();
        }, 12000)
      }
    }
  }
}

new p5(movementInSquare, window.document.getElementById('movement-in-square'));