# Recode 

[En]
This project proposes to rethink some of Bridget Riley's works with programming tools. It is more a question of rethinking than recoding these paintings. Some [archives](https://www.youtube.com/watch?v=hIPK6rvcscA&t=246s) suggest that Bridget Riley created these compositions using collages and fairly simple diagrams, so there is no initial code, or at least it is not available. 

My approach is to decode the painting, this activity is similar to reverse engineering as it is about understanding how to achieve this composition and perhaps why this composition strikes me so. I already knew when I saw this painting that it was not possible to solve it with a simple equation, yet I tried with my means to achieve it, but I think it was more a question of understanding how to obtain such a rich composition, with such a great amplitude, from such a poor lexicon of forms. This relationship, this back and forth between the code and the reproduction of the painting, is a bit like the discussion that I have never had and that I will probably never have with the artist.


---

[FR]
Ce projet propose de repenser certaines œuvres de Bridget Riley avec des outils de  programmation. Il s’agit plus de repenser donc que de recoder ces tableaux. Certaines [archives](https://www.youtube.com/watch?v=hIPK6rvcscA&t=246s) suggèrent que Bridget Riley créait ces compositions à l’aide de collages et de schémas assez simples, il n’y a donc pas de code initial ou du moins celui-ci n’est pas disponible. 

Ma démarche consiste à décoder le tableau, cette activité est similaire à de la rétro-ingénierie puisqu'il s’agit de comprendre comment obtenir cette composition et peut être aussi pourquoi cette composition me frappe autant. Je savais déjà en voyant ce tableau qu’il n’était pas possible de le résoudre à une simple équation, pourtant j’ai essayé avec mes moyens d’y parvenir mais je pense qu’il s’agissait plus de comprendre comment à partir d’un lexique de formes aussi pauvres obtenir une composition aussi riche, avec une si grande amplitude. Cette relation, ces aller retour  entre le code et la reproduction du tableau est un peu la discussion que je n’ai jamais eu et que je n’aurai sans doute jamais avec l’artiste.