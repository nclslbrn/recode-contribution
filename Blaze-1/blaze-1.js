// Blaze I by Bridget Riley
// Reproduction: https://external-preview.redd.it/XFotqhaqv4KUIR8AgtSkNqus6XJ6vTUtcZYrNwBh-v8.jpg?auto=webp&s=1f115de7e65f1acd844846cbac57341144ce4067
// MIT License - Nicolas Lebrun - @nclslbrn

let blaze1 = function (p) {
  let numRay, rayAngle, di, cntr, skew, sections
  const screenSize = Math.min(window.innerWidth, window.innerHeight),
    sketchSize = screenSize > 720 ? 720 : screenSize * 0.9,
    fade = 6

  function randomSections() {
    let r = Math.max(p.width, p.height) / 1.4
    let center = p.createVector(p.width / 2, p.height / 2)
    const sections = [{ r: r, ctr: center.copy() }]
    while (r > 8) {
      r *= p.random(0.6, 0.8)
      center.add(p.random(-0.1, 0.1) * r, p.random(-0.1, 0.1) * r)
      sections.push({ r: r, ctr: center.copy() })
    }
    return sections
  }

  p.setup = function () {
    p.createCanvas(sketchSize, sketchSize)
    p.noLoop()
    p.noStroke()

    sections = randomSections()
    numRay = Math.ceil(p.random(10, 16)) * 8
    rayAngle = p.TWO_PI / numRay
    di = rayAngle
    cntr = p.createVector(p.width / 2, p.height / 2)
    skew = 8
  }


  p.draw = function () {
    p.background(250)
    for (let h = 0; h < sections.length - 1; h++) {

      const prev = sections[h]
      const next = sections[h + 1]
      const nDi = (h % 2 === 0 ? -rayAngle : rayAngle) * skew

      let rayNum = 0

      for (let i = 0; i < p.TWO_PI; i += rayAngle) {

        const nI = i + rayAngle

        p.fill(rayNum % 2 === 0 ? 255 - h * fade : 50 + h * fade)

        p.beginShape()
        p.vertex(
          prev.ctr.x + Math.cos(i) * prev.r,
          prev.ctr.y + Math.sin(i) * prev.r
        )
        p.vertex(
          prev.ctr.x + Math.cos(nI) * prev.r,
          prev.ctr.y + Math.sin(nI) * prev.r
        )
        p.vertex(
          next.ctr.x + Math.cos(nI + nDi) * next.r,
          next.ctr.y + Math.sin(nI + nDi) * next.r
        )
        p.vertex(
          next.ctr.x + Math.cos(i + nDi) * next.r,
          next.ctr.y + Math.sin(i + nDi) * next.r
        )
        p.endShape(p.CLOSE)
        rayNum++;
      }
    }
  }

  p.mouseMoved = function () {
    const mX = Math.min(Math.max(0, p.mouseX), p.width)
    skew = Math.round((mX / p.width) * 10) * 2
    p.redraw()
  }

  p.mouseClicked = function () {
    sections = randomSections()
    p.redraw()
  }
}

new p5(blaze1, window.document.getElementById('blaze-1'));